package sk.saviq.simplerealestateappbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import sk.saviq.simplerealestateappbe.property.FileStorageProperties;

@SpringBootApplication
@EnableJpaAuditing
@EnableConfigurationProperties({FileStorageProperties.class})
public class SimpleRealEstateAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleRealEstateAppApplication.class, args);
    }
}
