package sk.saviq.simplerealestateappbe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sk.saviq.simplerealestateappbe.model.RealEstate;

@Repository
public interface RealEstateRepository extends JpaRepository<RealEstate, Long> {
}
