package sk.saviq.simplerealestateappbe.utils.enums;

public enum Planets {

    UNIDENTIFIED ("UNIDENTIFIED"),
    MERCURY ("MERCURY"),
    VENUS ("VENUS"),
    EARTH ("EARTH"),
    MARS ("MARS"),
    JUPITER ("JUPITER"),
    SATURN ("SATURN"),
    URANUS ("URANUS"),
    NEPTUNE ("NEPTUNE"),
    PLUTO ("PLUTO");

    private final String planet;

    Planets(String planet) {
        this.planet = planet;
    }
}
