package sk.saviq.simplerealestateappbe.utils.enums;

public enum RealEstateType {

    UNIDENTIFIED ("UNIDENTIFIED"),
    FLAT ("Flat"),
    HOUSE ("House");

    private String type;

    RealEstateType(String type) {
        this.type = type;
    }

}

