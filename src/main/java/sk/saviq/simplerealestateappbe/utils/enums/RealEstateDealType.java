package sk.saviq.simplerealestateappbe.utils.enums;


public enum RealEstateDealType {

    NONE("none"),
    HOT ("hot"),
    BEST ("best");

    private final String value;

    RealEstateDealType(String value) {
        this.value = value;
    }

}
