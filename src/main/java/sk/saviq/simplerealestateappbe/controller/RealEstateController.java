package sk.saviq.simplerealestateappbe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sk.saviq.simplerealestateappbe.exception.ResourceNotFoundException;
import sk.saviq.simplerealestateappbe.model.RealEstate;
import sk.saviq.simplerealestateappbe.repository.FileStorageService;
import sk.saviq.simplerealestateappbe.repository.RealEstateRepository;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class RealEstateController {

    private RealEstateRepository realEstateRepository;


    @Autowired
    public RealEstateController(RealEstateRepository realEstateRepository, FileStorageService fileStorageService) {
        this.realEstateRepository = realEstateRepository;
    }

    // Get all real estate notes
    @GetMapping("/estates")
    public List<RealEstate> getAllRealEstates() {
        return realEstateRepository.findAll();
    }

    // Get a Single Real Estate
    @GetMapping("estates/{id}")
    public RealEstate getRealEstateById(@PathVariable(value = "id") Long realEstateId) {
        return realEstateRepository.findById(realEstateId)
                .orElseThrow(() -> new ResourceNotFoundException("RealEstate", "id", realEstateId));
    }

    // Create a new Real Estate
    @PostMapping("/estates")
    public RealEstate createRealEstate(@Valid @RequestBody RealEstate realEstate) {
        return realEstateRepository.save(realEstate);
    }

    // Update Real Estate
    @PutMapping("estates/{id}")
    public RealEstate updateRealEstate(@PathVariable(value = "id") Long realEstateId,
        @Valid @RequestBody RealEstate realEstateDetails) {

        RealEstate realEstate = realEstateRepository.findById(realEstateId)
                .orElseThrow(() -> new ResourceNotFoundException("Real Estate", "id", realEstateId));

        updateRealEstateFields(realEstate, realEstateDetails);

        return realEstateRepository.save(realEstate);
    }

    // Delete a Real Estate
    @DeleteMapping("/estates/{id}")
    public ResponseEntity<?> deleteRealEstate(@PathVariable(value = "id") Long realEstateId) {
        RealEstate realEstate = realEstateRepository.findById(realEstateId)
                .orElseThrow(() -> new ResourceNotFoundException("Real Estate", "id", realEstateId));

        realEstateRepository.delete(realEstate);

        return ResponseEntity.ok().build();
    }

    private void updateRealEstateFields(RealEstate realEstate, RealEstate realEstateDetails) {
        realEstate.setTitle(realEstateDetails.getTitle());
        realEstate.setImageURL(realEstateDetails.getImageURL());
        realEstate.setPlanet(realEstateDetails.getPlanet());
        realEstate.setSector(realEstateDetails.getSector());
        realEstate.setType(realEstateDetails.getType());
        realEstate.setRooms(realEstateDetails.getRooms());
        realEstate.setGarage(realEstateDetails.getGarage());
        realEstate.setPrice(realEstateDetails.getPrice());
        realEstate.setDealType(realEstateDetails.getDealType());
    }

}
