package sk.saviq.simplerealestateappbe.model;

import sk.saviq.simplerealestateappbe.utils.enums.RealEstateDealType;
import sk.saviq.simplerealestateappbe.utils.enums.RealEstateType;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "real_estate")
public class RealEstate {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    private String title;

    private String imageUrl;

    @NotBlank
    private String planet;

    @NotBlank
    private String sector;

    @NotNull
    @Enumerated(STRING)
    private RealEstateType type;

    @NotNull
    private Integer rooms;

    @NotNull
    private Boolean garage;

    private String price;

    @Enumerated(STRING)
    private RealEstateDealType dealType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageURL() {
        return imageUrl;
    }

    public void setImageURL(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPlanet() {
        return planet;
    }

    public void setPlanet(String planet) {
        this.planet = planet;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public RealEstateType getType() {
        return type;
    }

    public void setType(RealEstateType type) {
        this.type = type;
    }

    public Integer getRooms() {
        return rooms;
    }

    public void setRooms(Integer rooms) {
        this.rooms = rooms;
    }

    public Boolean getGarage() {
        return garage;
    }

    public void setGarage(Boolean garage) {
        this.garage = garage;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public RealEstateDealType getDealType() {
        return dealType;
    }

    public void setDealType(RealEstateDealType dealType) {
        this.dealType = dealType;
    }
}
