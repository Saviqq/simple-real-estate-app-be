package sk.saviq.simplerealestateappbe.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@ResponseStatus(value = INTERNAL_SERVER_ERROR)
public class FileStorageException extends RuntimeException {

    private String message;

    public FileStorageException( String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
